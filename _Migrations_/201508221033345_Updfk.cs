namespace HLTERP.Migrations
{
    using System.Data.Entity.Migrations;
    
    public partial class Updfk : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("FollowUps", "Enquiry_id", "Enquiries");
            DropIndex("FollowUps", new[] { "Enquiry_id" });
            AlterColumn("FollowUps", "Enquiry_id", c => c.Int(nullable: false));
            AddForeignKey("FollowUps", "Enquiry_id", "Enquiries", "id", cascadeDelete: true);
            CreateIndex("FollowUps", "Enquiry_id");
            DropColumn("FollowUps", "enqid");
        }
        
        public override void Down()
        {
            AddColumn("FollowUps", "enqid", c => c.Int(nullable: false));
            DropIndex("FollowUps", new[] { "Enquiry_id" });
            DropForeignKey("FollowUps", "Enquiry_id", "Enquiries");
            AlterColumn("FollowUps", "Enquiry_id", c => c.Int());
            CreateIndex("FollowUps", "Enquiry_id");
            AddForeignKey("FollowUps", "Enquiry_id", "Enquiries", "id");
        }
    }
}
