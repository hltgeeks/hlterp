namespace HLTERP.Migrations
{
    using System.Data.Entity.Migrations;
    
    public partial class upd1 : DbMigration
    {
        public override void Up()
        {
            AlterColumn("Enquiries", "name", c => c.String(nullable: false));
            AlterColumn("Enquiries", "address", c => c.String(nullable: false));
            AlterColumn("Enquiries", "educationlevel", c => c.String(nullable: false));
            AlterColumn("Enquiries", "course", c => c.String(nullable: false));
            AlterColumn("Enquiries", "duration", c => c.String(nullable: false));
            AlterColumn("Enquiries", "joinPurpose", c => c.String(nullable: false));
            AlterColumn("Enquiries", "howKnownAboutHlt", c => c.String(nullable: false));
            AlterColumn("Enquiries", "timeRequested", c => c.String(nullable: false));
            AlterColumn("Enquiries", "demoFeedback", c => c.String(nullable: false));
            AlterColumn("Enquiries", "infrastructure", c => c.String(nullable: false));
            AlterColumn("Enquiries", "contactNum", c => c.String(nullable: false));
            AlterColumn("Enquiries", "emailId", c => c.String(nullable: false));
            AlterColumn("Enquiries", "enquiryHandledBy", c => c.String(nullable: false));
        }
        
        public override void Down()
        {
            AlterColumn("Enquiries", "enquiryHandledBy", c => c.String());
            AlterColumn("Enquiries", "emailId", c => c.String());
            AlterColumn("Enquiries", "contactNum", c => c.String());
            AlterColumn("Enquiries", "infrastructure", c => c.String());
            AlterColumn("Enquiries", "demoFeedback", c => c.String());
            AlterColumn("Enquiries", "timeRequested", c => c.String());
            AlterColumn("Enquiries", "howKnownAboutHlt", c => c.String());
            AlterColumn("Enquiries", "joinPurpose", c => c.String());
            AlterColumn("Enquiries", "duration", c => c.String());
            AlterColumn("Enquiries", "course", c => c.String());
            AlterColumn("Enquiries", "educationlevel", c => c.String());
            AlterColumn("Enquiries", "address", c => c.String());
            AlterColumn("Enquiries", "name", c => c.String());
        }
    }
}
