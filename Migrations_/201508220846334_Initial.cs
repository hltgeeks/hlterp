namespace HLTERP.Migrations
{
    using System.Data.Entity.Migrations;
    
    public partial class Initial : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("Followups", "Enquiry_id", "Enquiries");
            DropIndex("Followups", new[] { "Enquiry_id" });
            AddColumn("Enquiries", "enquiryHandledBy", c => c.String());
            AddColumn("Enquiries", "presentationGiven", c => c.String());
            AddColumn("Enquiries", "demoShown", c => c.String());
            AddColumn("Enquiries", "studentFeedback", c => c.String());
            AddColumn("Enquiries", "courseRecommended", c => c.String());
            AddColumn("Enquiries", "likeToJoin", c => c.String());
            AlterColumn("Enquiries", "date", c => c.DateTime(nullable: false));
            AlterColumn("Enquiries", "joinPurpose", c => c.String());
            AlterColumn("Enquiries", "howKnownAboutHlt", c => c.String());
            AlterColumn("Enquiries", "timeRequested", c => c.String());
            AlterColumn("Enquiries", "demoFeedback", c => c.String());
            AlterColumn("Enquiries", "contactNum", c => c.String());
            AlterColumn("Enquiries", "emailId", c => c.String());
            AlterColumn("Enquiries", "expectedDateOfJoin", c => c.DateTime(nullable: false));
            AlterColumn("FollowUps", "id", c => c.Int(nullable: false, identity: true));
            AlterColumn("FollowUps", "date", c => c.DateTime(nullable: false));
            AlterColumn("FollowUps", "comment", c => c.String());
            AlterColumn("FollowUps", "status", c => c.String());
            AlterColumn("FollowUps", "followId", c => c.Int(nullable: false));
            DropPrimaryKey("Followups", new[] { "Id" });
            AddPrimaryKey("FollowUps", "id");
            AddForeignKey("FollowUps", "Enquiry_id", "Enquiries", "id");
            CreateIndex("FollowUps", "Enquiry_id");
        }
        
        public override void Down()
        {
            DropIndex("FollowUps", new[] { "Enquiry_id" });
            DropForeignKey("FollowUps", "Enquiry_id", "Enquiries");
            DropPrimaryKey("FollowUps", new[] { "id" });
            AddPrimaryKey("Followups", "Id");
            AlterColumn("FollowUps", "Followid", c => c.Int(nullable: false));
            AlterColumn("FollowUps", "Status", c => c.String());
            AlterColumn("FollowUps", "Comment", c => c.String());
            AlterColumn("FollowUps", "Date", c => c.DateTime(nullable: false));
            AlterColumn("FollowUps", "Id", c => c.Int(nullable: false, identity: true));
            AlterColumn("Enquiries", "expecteddateofjoin", c => c.String());
            AlterColumn("Enquiries", "emailid", c => c.String());
            AlterColumn("Enquiries", "contactnum", c => c.String());
            AlterColumn("Enquiries", "demofeedback", c => c.String());
            AlterColumn("Enquiries", "timerequested", c => c.String());
            AlterColumn("Enquiries", "howknownabouthlt", c => c.String());
            AlterColumn("Enquiries", "joinpurpose", c => c.String());
            AlterColumn("Enquiries", "date", c => c.String());
            DropColumn("Enquiries", "likeToJoin");
            DropColumn("Enquiries", "courseRecommended");
            DropColumn("Enquiries", "studentFeedback");
            DropColumn("Enquiries", "demoShown");
            DropColumn("Enquiries", "presentationGiven");
            DropColumn("Enquiries", "enquiryHandledBy");
            CreateIndex("Followups", "Enquiry_id");
            AddForeignKey("Followups", "Enquiry_id", "Enquiries", "id");
        }
    }
}
