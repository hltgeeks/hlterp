namespace HLTERP.Migrations
{
    using System.Data.Entity.Migrations;
    
    public partial class Init : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "Enquiries",
                c => new
                    {
                        id = c.Int(nullable: false, identity: true),
                        name = c.String(),
                        date = c.String(),
                        address = c.String(),
                        educationlevel = c.String(),
                        course = c.String(),
                        duration = c.String(),
                        joinpurpose = c.String(),
                        howknownabouthlt = c.String(),
                        timerequested = c.String(),
                        demofeedback = c.String(),
                        infrastructure = c.String(),
                        contactnum = c.String(),
                        emailid = c.String(),
                        expecteddateofjoin = c.String(),
                    })
                .PrimaryKey(t => t.id);
            
            CreateTable(
                "Followups",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Date = c.DateTime(nullable: false),
                        Comment = c.String(),
                        Status = c.String(),
                        Followid = c.Int(nullable: false),
                        Enquiry_id = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("Enquiries", t => t.Enquiry_id)
                .Index(t => t.Enquiry_id);
            
            CreateTable(
                "Invoices",
                c => new
                    {
                        receiptNo = c.Int(nullable: false, identity: true),
                        name = c.String(),
                        address = c.String(),
                        date = c.DateTime(nullable: false),
                        contactNum = c.Int(nullable: false),
                        emailId = c.String(),
                        particular = c.String(),
                        normalPrice = c.Double(nullable: false),
                        offeredPrice = c.Double(nullable: false),
                        totalAmount = c.Double(nullable: false),
                        balanceAmount = c.Double(nullable: false),
                        modeOfPayment = c.String(),
                        dueDate = c.DateTime(nullable: false),
                        termsAndCondition = c.String(),
                        receviedBy = c.String(),
                    })
                .PrimaryKey(t => t.receiptNo);
            
            CreateTable(
                "Paidins",
                c => new
                    {
                        id = c.Int(nullable: false, identity: true),
                        Date = c.DateTime(nullable: false),
                        Course = c.String(),
                        Payername = c.String(),
                        Paidamount = c.String(),
                        Receivername = c.String(),
                        receiptNo = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.id);
            
            CreateTable(
                "Paidouts",
                c => new
                    {
                        id = c.Int(nullable: false, identity: true),
                        date = c.DateTime(nullable: false),
                        reason = c.String(),
                        amount = c.Double(nullable: false),
                        paidby = c.String(),
                        paidto = c.String(),
                    })
                .PrimaryKey(t => t.id);
            
        }
        
        public override void Down()
        {
            DropIndex("Followups", new[] { "Enquiry_id" });
            DropForeignKey("Followups", "Enquiry_id", "Enquiries");
            DropTable("Paidouts");
            DropTable("Paidins");
            DropTable("Invoices");
            DropTable("Followups");
            DropTable("Enquiries");
        }
    }
}
