using HLTERP.Models;
namespace HLTERP.Migrations
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;
    using System.Web.Security;
    using WebMatrix.WebData;

    internal sealed class Configuration : DbMigrationsConfiguration<HLTERPDB>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = true;
        }

        protected override void Seed(HLTERPDB context)
        {
            SeedMembership();
        }

        private void SeedMembership()
        {
            WebSecurity.InitializeDatabaseConnection("DefaultConnection", "UserProfile", "UserId", "UserName", autoCreateTables: true);
            var roles = (SimpleRoleProvider)Roles.Provider;
            var membership = (SimpleMembershipProvider)Membership.Provider;
            if (!roles.RoleExists("ADMIN")) {

                Roles.CreateRole("ADMIN");
            
            }
            if (!roles.RoleExists("USER"))
            {

                Roles.CreateRole("USER");

            } if (!roles.RoleExists("FACULTY"))
            {

                Roles.CreateRole("FACULTY");

            }

            if (membership.GetUser("hltadmin", false) == null) {

                membership.CreateUserAndAccount("hltadmin", "HLT@dm!n");
            
            }
            if (!roles.GetRolesForUser("hltadmin").Contains("ADMIN")) {

                roles.AddUsersToRoles(new[] { "hltadmin" }, new[] { "ADMIN" });
            
            }

        }
    }
}
