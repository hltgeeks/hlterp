namespace HLTERP.Migrations
{
    using System.Data.Entity.Migrations;
    
    public partial class upd11 : DbMigration
    {
        public override void Up()
        {
            AddColumn("FollowUps", "enqid", c => c.Int(nullable: false));
            DropColumn("FollowUps", "followId");
        }
        
        public override void Down()
        {
            AddColumn("FollowUps", "followId", c => c.Int(nullable: false));
            DropColumn("FollowUps", "enqid");
        }
    }
}
