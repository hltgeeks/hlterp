﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace HLTERP.Models
{
    public class HLTERPDB:DbContext
    {
        public HLTERPDB(): base("name=DefaultConnection")
    {

    }
        public DbSet<Enquiry> Enquires { get; set; }
       public DbSet<FollowUp> FollowUps { get; set; }
        public DbSet<Invoice> Invoices { get; set; }
        public DbSet<InvoiceBilling> InvoiceBilling { get; set; }
        //public DbSet<Admin> admin { get; set; }
        public DbSet<Paidin> PaidIns { get; set; }
        public DbSet<Paidout> PaidOuts { get; set; }
        public DbSet<StartBatch> StartBatches { get; set; }
        public DbSet<Student> Students { get; set; }
        public DbSet<Faculty> Faculties { get; set; }
        public DbSet<Course> Courses { get; set; }
        public DbSet<Admission> Admissions  { get; set; }
        public DbSet<StartBatchAndCourse> StartBatchAndCourses { get; set; }

        public DbSet<UserProfile> UserProfiles { get; set; }
        
    }
}