﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace HLTERP.Models
{
    public class Course
    {
        [Key]
        public int CourseId { get; set; }
        [Required(ErrorMessage="Please enter course name")]
        [MaxLength(20)]
        public string CourseName { get; set; }
        [Required(ErrorMessage="Please enter faculty name")]
        public string FacultyName { get; set; }
      
    }
}