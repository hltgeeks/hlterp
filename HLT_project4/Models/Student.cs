﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace HLTERP.Models
{
    public class Student
    {
        [Key]
        public int StudentId { get; set; }

        [Required(ErrorMessage="Enter the name of the Student ")]
        [Display(Name = "Student Name")]
        public string StudentName { get; set; }

        [Required(ErrorMessage="select Course name from the list")]
        [Display(Name = "Course Name")]
        public string CourseName { get; set; }

        [Required(ErrorMessage="Select fron the list")]
        public string EducationLevel { get; set; }

        [Required(ErrorMessage="Enter the College name")]
        [Display(Name = "College Name")]
        public string CollegeName { get; set; }

        [Required]
        [Display(Name = "Contact Number")]
        [DataType(DataType.PhoneNumber)]
        public string contactNum { get; set; }



        [Required(ErrorMessage="Enter the Address")]
        [Display(Name = "Student Address")]
        public string StudentAddress { get; set; }

        [Required(ErrorMessage="Enter your Date of birth")]
        [DataType(DataType.Date)]
        [Display(Name = "Date Of Birth")]
        public DateTime StudentDOB { get; set; }
        [Required(ErrorMessage="Enter your age")]
        [Display(Name = "Student Age")]
        public int StudentAge { get; set; }

        //public virtual ICollection<Admission> admissions { get; set; }
    }
}