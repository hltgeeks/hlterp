﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading;
using System.Timers;
using System.Web;

namespace HLTERP.Models
{
    public class Batchprogress
    {
        [Key]

        public int BatchID { get; set; }
        [Required(ErrorMessage = "Enter the name of the Batch ")]
        [Display(Name = "Batch Name")]
        public DateTime Date { get; set; }
        [Required(ErrorMessage = "Enter the start tim of the Batch ")]
        [Display(Name = "Start Time")]
        public DateTime StartTime { get; set; }
        [Required(ErrorMessage = "Enter the end time of the Batch ")]
        [Display(Name = "End Time")]
        public DateTime EndTime { get; set; }
        [Required(ErrorMessage = "Enter the name of the Student ")]
        [Display(Name = "Student Name")]
        public string StudentName { get; set; }
        public string FacultyName { get; set; }
        [Required(ErrorMessage = "Enter the name of the Concept ")]
        [Display(Name = "Concept Name")]
        public string ConceptsCovered { get; set; }
        public string Comments { get; set; }

}
}