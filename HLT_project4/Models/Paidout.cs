﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace HLTERP.Models
{
    public class Paidout
    {
        [Key]
        public int id { get; set; }
        [Required(ErrorMessage="Enter Date")]
        [Display(Name="Date")]
        [DataType(DataType.Date)]
        public DateTime Date { get; set; }

        [Required(ErrorMessage="enter the reason for Expenses")]
        [Display(Name="Reason")]
        public string Reason { get; set; }

        [Required (ErrorMessage="Enter the amount You paid for above reason" )]
        [Display(Name="Amount")]
        public double Amount { get; set; }

        [Required(ErrorMessage="Enter the name of the Payer")]
        [Display(Name="Paid By")]
        public string PaidBy { get; set; }

        [Required(ErrorMessage="Enter the name to whom you paid")]
        [Display(Name="Paid To")]
        public string PaidTo { get; set; }

    }
}