﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace HLTERP.Models
{
    
    public class Invoice
    {
        
        //public int InId { get; set; }
        [Key]
        [MaxLength(5)]
        [Required(ErrorMessage="Enter ReceiptNo")]
        public string ReceiptNo { get; set; }
        [Required(ErrorMessage="Please enter the name")]
        public string Name { get; set; }
        [Required(ErrorMessage="Please enter Adderess")]
        public string Address { get; set; }
        [Required(ErrorMessage="enter date")]
        [DataType(DataType.Date)]
        public DateTime Date { get; set; }

        [Required]
        [Display(Name = "Contact Number")]
        [DataType(DataType.PhoneNumber)]
        public string ContactNum { get; set; }

        [Required(ErrorMessage="Enter Email id")]
        [DataType(DataType.EmailAddress)]
        public string EmailId { get; set; }

        public string Particular { get; set; }

        public double NormalPrice { get; set; }
        public double OfferedPrice { get; set; }
        public double TotalAmount { get; set; }
        public double BalanceAmount{ get; set; }
        public string ModeOfPayment { get; set; }
        [Required(ErrorMessage="please mention Due Date")]
        [DataType(DataType.Date)]
        public DateTime DueDate { get; set; }
        public string TermsAndCondition { get; set; }

        public string ReceviedBy { get; set; }


    }
}