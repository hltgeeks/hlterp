﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace HLTERP.Models
{
    public class StartBatch
    {
        [Key]
        public int BatchId { get; set;}
        [Required(ErrorMessage="Please enter Batch Name")]
        [Display(Name="Batch Name")]
        public string BatchName { get; set; }
        [Required(ErrorMessage="Select course from list")]
        [Display(Name = "Course Name")]
        public string CourseName { get; set; }
        [Required(ErrorMessage="Select Faculty name from list")]
        [Display(Name = "Faculty Name")]
        public string FacultyName { get; set; }
        [Required(ErrorMessage="Enter the time of batch start batch")]
        [DataType(DataType.Date)]
        [Display(Name = "From Time")]
        public DateTime FromTime { get; set; }
        [Required(ErrorMessage="Enter the time of End Batch")]
        [Display(Name = "To Time")]
        [DataType(DataType.Date)]
        public DateTime ToTime { get; set; }
        [Required(ErrorMessage="Enter the date batch started")]
        [Display(Name = "Start Date")]
        [DataType(DataType.Date)]
        public DateTime StartDate { get; set; }
        [Required(ErrorMessage="Enter the expected date of ending batch") ]
        [DataType(DataType.Date)]
        [Display(Name = "Expected end Date")]
        public DateTime ExpectedEndDate { get; set; }

        public ICollection<Student> Student { get; set; }
        public Faculty Faculty { get; set; }
        public Course Course { get; set; }
        public ICollection<int> SelectedCourseList { get;set;}
        public ICollection<StartBatchAndCourse> StartBatchAndCourse { get; set; }

    }
}