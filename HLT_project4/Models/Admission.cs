﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace HLTERP.Models
{
    public class Admission
    {
        [Key]
        public int Admissionid { get; set; }
        [Required (ErrorMessage ="ErrorMeter Admission date")]
        public int date  { get; set; } 
        [Required (ErrorMessage ="Enter Admission cources")]
        public string cources { get; set; }
        [Required (ErrorMessage ="Enter student name")]
        public string student { get; set; }
        [Required (ErrorMessage ="invoice")]
        public string invoice { get; set; }

       }
}