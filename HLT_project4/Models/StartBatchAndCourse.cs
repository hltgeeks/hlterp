﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace HLTERP.Models
{
    public class StartBatchAndCourse
    {
        [Key]
        public int BatchCourseId { get; set; }

        //ForiegnKey to Course table for saving selected courseId's
        public int CourseId { get; set; }
        public Course Course { get; set; }

        //ForiegnKey to Batch table for saving selected BatchId's
        public int BatchId{get;set;}
        public StartBatch StartBatch{get;set;}

    }
}