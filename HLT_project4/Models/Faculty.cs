﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace HLTERP.Models
{
    public class Faculty
    {
        [Key]
        public int FacultyId { get; set;}
        [Required(ErrorMessage="Enter faculty name")]
        public string FacultyName { get; set; }
       [Required(ErrorMessage="Enter the qualification of Faculty ")]
        public string FacultyQulification { get; set; }
        [Required(ErrorMessage="Enter Experience of the faculty in number of years ")]
        public int Experiance { get; set; }
        [Required(ErrorMessage="Enter Email id ")]
        [DataType(DataType.EmailAddress)]
        public string EmailId { get; set; }
        [Required(ErrorMessage = "Enter Contact number ")]
        [DataType(DataType.PhoneNumber)]
        public int ContactNum { get; set; }
        [Required(ErrorMessage="Enter Adderess")]
        public string Adderess { get; set; }
        [Required(ErrorMessage="Enter salary offered ")]
        public double SalaryOffered { get; set; }
        [Required(ErrorMessage="Enter the batch handling")]
        public string BatchHandling { get; set; }
        [Required(ErrorMessage="Enter course handling ")]
        public string CourseHandling { get; set; }
        public virtual ICollection<StartBatch> StartBatch { set; get; }
      //  public virtual ICollection<Course> course { set; get; }

    }
}