﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace HLTERP.Models
{
    public class Device
    {
        [Key]

        public int ID { get; set; }
        [Required(ErrorMessage = "Enter the name of the Device ")]
        [Display(Name = "Device Name")]
        public string Name { get; set; }
        public string Models { get; set; }
    }
}