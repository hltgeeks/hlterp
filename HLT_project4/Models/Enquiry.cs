﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace HLTERP.Models
{
    public class Enquiry
    {
        private DateTime date=DateTime.Now;
        [Key]
        public int Id { get; set; }
        [Required(ErrorMessage="please enter the name of Student")]
        [Display(Name="Name")]
        [DataType(DataType.Text)]
        public string Name { get; set; }

        [Required(ErrorMessage="please enter the date")]
        [Display(Name = "Date")]
        [DataType(DataType.Date)]
        public DateTime Date { get {

            return date;

        }
            set {

                date = value;
            
            }
        }
        [Required(ErrorMessage="please enter the Address")]
        [Display(Name = "Address")]
        public string Address { get; set; }
        [Required(ErrorMessage="Select any one option")]
        [Display(Name = "Education Level")]
        public string EducationLevel { get; set; }
        [Required(ErrorMessage="Select atleast one option")]
        [Display(Name = "Course")]
        public string Course { get; set; }

        [Required(ErrorMessage="please enter the duration of course")]
        [Display(Name = "Duration in Months")]
        [DataType(DataType.Duration)]
        [Range(1,12)]
        public string Duration { get; set; }
        [Required(ErrorMessage="enter the purpose of join")]
        [Display(Name = "Join Purpose")]
        public string JoinPurpose { get; set; }
        [Required]
        [Display(Name = "How u Know About HLT")]
        public string HowKnownAboutHlt { get; set; }
        [Required(ErrorMessage="Enter your convenient time")]
        [Display(Name = "Time Requested")]
        [DataType(DataType.Time)]
        public string TimeRequested { get; set; }
        [Required]
        [Display(Name = "Demo FeedBack")]
        public string DemoFeedback { get; set; }
        [Required]
        [Display(Name = "Infrastructure")]
        public string Infrastructure { get; set; }
        [Required]
        [Display(Name = "Contact Number")]
        [DataType(DataType.PhoneNumber)]
        public string ContactNum { get; set; }
        [Required]
        [Display(Name="Email ID")]
        [DataType(DataType.EmailAddress)]
        public string EmailId { get; set; }
        [Required]
        [DataType(DataType.Date)]
        [Display(Name = "Expected Date of Joining")]
        public DateTime ExpectedDateOfJoin { get; set; }
        [Required]
        [Display(Name = "Enquiry Handled By")]
        public string EnquiryHandledBy { get; set; }

        [Required(ErrorMessage="Select one option")] 
        [Display(Name="Presentation Given?")]
        public string PresentationGiven { get; set; }
        [Required(ErrorMessage="Select one option")]
        [Display(Name="Demo Shown?")]
        public string DemoShown { get; set; }
        [Required(ErrorMessage="Select one option")]
        [Display(Name="Student FeedBack")]
        public string StudentFeedback { get; set; }
        [Required(ErrorMessage="Need to mention any one of the course" )] 
        [Display(Name="Course Recommended")]
        public string CourseRecommended { get; set; }

        [Required(ErrorMessage="Select any one option")]
        [Display(Name="Like To Join?")]
        public string LikeToJoin { get; set; }
        [Display(Name="Reason")]
        public string Reason{get;set;}
        //  public Course course { get; set; }
        public virtual ICollection<FollowUp> followup { get; set; }
      
    }
}