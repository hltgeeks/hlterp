﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

using System.Linq;
using System.Web;
using HLTERP.Models;
using System.ComponentModel.DataAnnotations.Schema;

namespace HLTERP.Models
{
    public class FollowUp
    {
        [Key]
        [DatabaseGeneratedAttribute(DatabaseGeneratedOption.Identity)]
        public int id { get; set; }
        [Required(ErrorMessage="Enter the Date")]
        [DataType(DataType.Date)]
        public DateTime date { get; set; }
        

        public string comment { get; set; }
        [Required]
        public String status { get; set; }

       
        public int Enquiry_id { get; set; }
       
        [ForeignKey("Enquiry_id")]

        public Enquiry Enq { get; set; }


    }
}