﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace HLTERP.Models
{

    public class Paidin
    {
        [Key]
        public int Id {get;set;}
        [Required(ErrorMessage="Enter Date")]
        [DataType(DataType.Date)]
        [Display(Name="Date")]
        public DateTime Date { get; set; }
        [Required(ErrorMessage="Enter course name")]
        [Display(Name="Course")]
        public string Course { get; set; }
        [Required(ErrorMessage="Enter payer name")]
        [Display(Name="Payer Name")]
        public string PayerName { get; set; }
        [Display(Name="Paid Amount")]
        [Required(ErrorMessage="Enter amount to be paid")]
        public Double  PaidAmount { get; set; }
        [Display(Name="Receiver Name")]
        [Required(ErrorMessage="Enter the name of the receiver")]
        public string ReceiverName {get; set;}
        
        public string ReceiptNo { get; set; }
    }
}