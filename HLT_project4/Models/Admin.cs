﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace HLTERP.Models
{
    public class Admin
    {

        [Key]
        public int aId { get; set; }
        [Required(ErrorMessage = "Please enter user name")]

        [Display(Name = "User Name")]
        public string userName { get; set; }

        [Required(ErrorMessage = "please enter password")]
        [Display(Name = "Password")]
        [DataType(DataType.Password)]
        public string password { get; set; }

    }
}