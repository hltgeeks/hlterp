﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace HLTERP.Models
{
    public class FileUpload
    {
        [Key]
        [Required]
        public HttpPostedFileBase File { get; set; }
    }
}