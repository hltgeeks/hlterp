﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace HLTERP.Models
{
    public class Employee
    {
        [Key]
        

        public int ID { get; set; }

        [Required(ErrorMessage = "Enter the name of the Employee ")]
        [Display(Name = "Employee Name")]
        public string Name { get; set; }

        public double Salary { get; set; }
    }
}