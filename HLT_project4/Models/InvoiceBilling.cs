﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace HLTERP.Models
{
    public class InvoiceBilling
    {
         [Key]
        public int InId { get; set; }
        public int ReceiptNo { get; set; }
        [Required(ErrorMessage="Please enter the name")]
        public string Name { get; set; }
        [Required(ErrorMessage="Please enter Adderess")]
        public string Address { get; set; }
        [Required(ErrorMessage="enter date")]
        [DataType(DataType.Date)]
        public DateTime Date { get; set; }

        [Required]
        [Display(Name = "Contact Number")]
        [DataType(DataType.PhoneNumber)]
        public string ContactNum { get; set; }

        [Required(ErrorMessage="Enter Email id")]
        [DataType(DataType.EmailAddress)]
        [Display(Name = "Email Id")]
        public string EmailId { get; set; }

          [Display(Name = "Particular")]
        [Required(ErrorMessage="Please Enter the Particular of payment")]
        public string Particular { get; set; }

          [Display(Name = "Normal Price")]
          [Required(ErrorMessage = "Please Enter the Normal Price")]
        public double NormalPrice { get; set; }

          [Display(Name = "Offered Price")]
          [Required(ErrorMessage = "Please Enter the Offered Price ")]
        public double OfferedPrice { get; set; }

          [Display(Name = "Total Amount")]
          
        public double TotalAmount { get; set; }
          [Display(Name = "Balance Amount")]
        public double BalanceAmount{ get; set; }

          [Display(Name = "Mode of payment")]
          [Required(ErrorMessage = "Please Enter the MOde of payment either by cash,cheque,etc..")]
        public string ModeOfPayment { get; set; }


        [Required(ErrorMessage="please mention Due Date")]
        [DataType(DataType.Date)]
          [Display(Name = "Due Date")]
      
        public DateTime DueDate { get; set; }
        [Display(Name="Recevied By")]
        public string ReceviedBy { get; set; }


        [MustBeTrue(ErrorMessage = "Please Accept the Terms & Conditions")]
        [Display(Name = "Terms And Condition")]
        public bool TermsAndCondition { get; set; } }
        //Making Custom attribute for validating checkbox 
        // IClientValidatable for client side Validation
       public class MustBeTrueAttribute : ValidationAttribute, IClientValidatable 
       { 
       public override bool IsValid(object value) 
       { 
       return value is bool && (bool)value; 
       }
       // Implement IClientValidatable for client side Validation 
     public IEnumerable<ModelClientValidationRule> GetClientValidationRules(ModelMetadata metadata, ControllerContext context)
       {
      return new ModelClientValidationRule[] {
       new ModelClientValidationRule { ValidationType = "checkboxtrue", ErrorMessage = this.ErrorMessage } };
       } 
       } 

        


    }
 
