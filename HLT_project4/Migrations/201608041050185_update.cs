namespace HLTERP.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class update : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Courses",
                c => new
                    {
                        CourseId = c.Int(nullable: false, identity: true),
                        CourseName = c.String(nullable: false, maxLength: 20),
                        FacultyName = c.String(nullable: false),
                    })
                .PrimaryKey(t => t.CourseId);
            
            CreateTable(
                "dbo.Enquiries",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(nullable: false),
                        Date = c.DateTime(nullable: false),
                        Address = c.String(nullable: false),
                        EducationLevel = c.String(nullable: false),
                        Course = c.String(nullable: false),
                        Duration = c.String(nullable: false),
                        JoinPurpose = c.String(nullable: false),
                        HowKnownAboutHlt = c.String(nullable: false),
                        TimeRequested = c.String(nullable: false),
                        DemoFeedback = c.String(nullable: false),
                        Infrastructure = c.String(nullable: false),
                        ContactNum = c.String(nullable: false),
                        EmailId = c.String(nullable: false),
                        ExpectedDateOfJoin = c.DateTime(nullable: false),
                        EnquiryHandledBy = c.String(nullable: false),
                        PresentationGiven = c.String(nullable: false),
                        DemoShown = c.String(nullable: false),
                        StudentFeedback = c.String(nullable: false),
                        CourseRecommended = c.String(nullable: false),
                        LikeToJoin = c.String(nullable: false),
                        Reason = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.FollowUps",
                c => new
                    {
                        id = c.Int(nullable: false, identity: true),
                        date = c.DateTime(nullable: false),
                        comment = c.String(),
                        status = c.String(nullable: false),
                        Enquiry_id = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.id)
                .ForeignKey("dbo.Enquiries", t => t.Enquiry_id, cascadeDelete: true)
                .Index(t => t.Enquiry_id);
            
            CreateTable(
                "dbo.Faculties",
                c => new
                    {
                        FacultyId = c.Int(nullable: false, identity: true),
                        FacultyName = c.String(nullable: false),
                        FacultyQulification = c.String(nullable: false),
                        Experiance = c.Int(nullable: false),
                        EmailId = c.String(nullable: false),
                        ContactNum = c.Int(nullable: false),
                        Adderess = c.String(nullable: false),
                        SalaryOffered = c.Double(nullable: false),
                        BatchHandling = c.String(nullable: false),
                        CourseHandling = c.String(nullable: false),
                    })
                .PrimaryKey(t => t.FacultyId);
            
            CreateTable(
                "dbo.StartBatches",
                c => new
                    {
                        BatchId = c.Int(nullable: false, identity: true),
                        BatchName = c.String(nullable: false),
                        CourseName = c.String(nullable: false),
                        FacultyName = c.String(nullable: false),
                        FromTime = c.DateTime(nullable: false),
                        ToTime = c.DateTime(nullable: false),
                        StartDate = c.DateTime(nullable: false),
                        ExpectedEndDate = c.DateTime(nullable: false),
                        Course_CourseId = c.Int(),
                        Faculty_FacultyId = c.Int(),
                    })
                .PrimaryKey(t => t.BatchId)
                .ForeignKey("dbo.Courses", t => t.Course_CourseId)
                .ForeignKey("dbo.Faculties", t => t.Faculty_FacultyId)
                .Index(t => t.Course_CourseId)
                .Index(t => t.Faculty_FacultyId);
            
            CreateTable(
                "dbo.StartBatchAndCourses",
                c => new
                    {
                        BatchCourseId = c.Int(nullable: false, identity: true),
                        CourseId = c.Int(nullable: false),
                        BatchId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.BatchCourseId)
                .ForeignKey("dbo.Courses", t => t.CourseId, cascadeDelete: true)
                .ForeignKey("dbo.StartBatches", t => t.BatchId, cascadeDelete: true)
                .Index(t => t.CourseId)
                .Index(t => t.BatchId);
            
            CreateTable(
                "dbo.Students",
                c => new
                    {
                        StudentId = c.Int(nullable: false, identity: true),
                        StudentName = c.String(nullable: false),
                        CourseName = c.String(nullable: false),
                        EducationLevel = c.String(nullable: false),
                        CollegeName = c.String(nullable: false),
                        contactNum = c.String(nullable: false),
                        StudentAddress = c.String(nullable: false),
                        StudentDOB = c.DateTime(nullable: false),
                        StudentAge = c.Int(nullable: false),
                        StartBatch_BatchId = c.Int(),
                    })
                .PrimaryKey(t => t.StudentId)
                .ForeignKey("dbo.StartBatches", t => t.StartBatch_BatchId)
                .Index(t => t.StartBatch_BatchId);
            
            CreateTable(
                "dbo.InvoiceBillings",
                c => new
                    {
                        InId = c.Int(nullable: false, identity: true),
                        ReceiptNo = c.Int(nullable: false),
                        Name = c.String(nullable: false),
                        Address = c.String(nullable: false),
                        Date = c.DateTime(nullable: false),
                        ContactNum = c.String(nullable: false),
                        EmailId = c.String(nullable: false),
                        Particular = c.String(nullable: false),
                        NormalPrice = c.Double(nullable: false),
                        OfferedPrice = c.Double(nullable: false),
                        TotalAmount = c.Double(nullable: false),
                        BalanceAmount = c.Double(nullable: false),
                        ModeOfPayment = c.String(nullable: false),
                        DueDate = c.DateTime(nullable: false),
                        ReceviedBy = c.String(),
                        TermsAndCondition = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.InId);
            
            CreateTable(
                "dbo.Invoices",
                c => new
                    {
                        ReceiptNo = c.String(nullable: false, maxLength: 5),
                        Name = c.String(nullable: false),
                        Address = c.String(nullable: false),
                        Date = c.DateTime(nullable: false),
                        ContactNum = c.String(nullable: false),
                        EmailId = c.String(nullable: false),
                        Particular = c.String(),
                        NormalPrice = c.Double(nullable: false),
                        OfferedPrice = c.Double(nullable: false),
                        TotalAmount = c.Double(nullable: false),
                        BalanceAmount = c.Double(nullable: false),
                        ModeOfPayment = c.String(),
                        DueDate = c.DateTime(nullable: false),
                        TermsAndCondition = c.String(),
                        ReceviedBy = c.String(),
                    })
                .PrimaryKey(t => t.ReceiptNo);
            
            CreateTable(
                "dbo.Paidins",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Date = c.DateTime(nullable: false),
                        Course = c.String(nullable: false),
                        PayerName = c.String(nullable: false),
                        PaidAmount = c.Double(nullable: false),
                        ReceiverName = c.String(nullable: false),
                        ReceiptNo = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Paidouts",
                c => new
                    {
                        id = c.Int(nullable: false, identity: true),
                        Date = c.DateTime(nullable: false),
                        Reason = c.String(nullable: false),
                        Amount = c.Double(nullable: false),
                        PaidBy = c.String(nullable: false),
                        PaidTo = c.String(nullable: false),
                    })
                .PrimaryKey(t => t.id);
            
            CreateTable(
                "dbo.UserProfile",
                c => new
                    {
                        UserId = c.Int(nullable: false, identity: true),
                        UserName = c.String(),
                    })
                .PrimaryKey(t => t.UserId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Students", "StartBatch_BatchId", "dbo.StartBatches");
            DropForeignKey("dbo.StartBatchAndCourses", "BatchId", "dbo.StartBatches");
            DropForeignKey("dbo.StartBatchAndCourses", "CourseId", "dbo.Courses");
            DropForeignKey("dbo.StartBatches", "Faculty_FacultyId", "dbo.Faculties");
            DropForeignKey("dbo.StartBatches", "Course_CourseId", "dbo.Courses");
            DropForeignKey("dbo.FollowUps", "Enquiry_id", "dbo.Enquiries");
            DropIndex("dbo.Students", new[] { "StartBatch_BatchId" });
            DropIndex("dbo.StartBatchAndCourses", new[] { "BatchId" });
            DropIndex("dbo.StartBatchAndCourses", new[] { "CourseId" });
            DropIndex("dbo.StartBatches", new[] { "Faculty_FacultyId" });
            DropIndex("dbo.StartBatches", new[] { "Course_CourseId" });
            DropIndex("dbo.FollowUps", new[] { "Enquiry_id" });
            DropTable("dbo.UserProfile");
            DropTable("dbo.Paidouts");
            DropTable("dbo.Paidins");
            DropTable("dbo.Invoices");
            DropTable("dbo.InvoiceBillings");
            DropTable("dbo.Students");
            DropTable("dbo.StartBatchAndCourses");
            DropTable("dbo.StartBatches");
            DropTable("dbo.Faculties");
            DropTable("dbo.FollowUps");
            DropTable("dbo.Enquiries");
            DropTable("dbo.Courses");
        }
    }
}
