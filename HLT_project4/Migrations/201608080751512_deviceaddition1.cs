namespace HLTERP.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class deviceaddition1 : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Devices",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        Name = c.String(nullable: false),
                        Models = c.String(),
                    })
                .PrimaryKey(t => t.ID);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.Devices");
        }
    }
}
