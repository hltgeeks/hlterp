namespace HLTERP.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class batchprogressaddition : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Batchprogresses",
                c => new
                    {
                        BatchID = c.Int(nullable: false, identity: true),
                        Date = c.DateTime(nullable: false),
                        StartTime = c.DateTime(nullable: false),
                        EndTime = c.DateTime(nullable: false),
                        StudentName = c.String(),
                        FacultyName = c.String(),
                        ConceptsOverall = c.String(),
                        Comments = c.String(),
                    })
                .PrimaryKey(t => t.BatchID);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.Batchprogresses");
        }
    }
}
