namespace HLTERP.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class batchprogressaddition1 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Batchprogresses", "ConceptsCovered", c => c.String(nullable: false));
            AlterColumn("dbo.Batchprogresses", "StudentName", c => c.String(nullable: false));
            DropColumn("dbo.Batchprogresses", "ConceptsOverall");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Batchprogresses", "ConceptsOverall", c => c.String());
            AlterColumn("dbo.Batchprogresses", "StudentName", c => c.String());
            DropColumn("dbo.Batchprogresses", "ConceptsCovered");
        }
    }
}
