﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using HLTERP.Models;
using PagedList;

namespace HLTERP.Controllers
{
    public class InvoiceBillingController : Controller
    {
        private HLTERPDB db = new HLTERPDB();

        //
        // GET: /InvoiceBilling/

        public ActionResult Index(string name = null, int page = 1)
        {
            var model = db.InvoiceBilling
                .OrderByDescending(r => r.Date)
                .Where(r => name == null || r.Name.Contains(name))
                .ToPagedList(page, 5);



        
            if (Request.IsAjaxRequest())
            {

                return PartialView("_Enquiries", model);

            }
            return View(model);
                //db.InvoiceBilling
                //   .OrderByDescending(r => r.Name)
                //   .Where(r => searchTerm == null || r.Name.StartsWith(searchTerm))
                //   .Select(r => new InvoiceBilling
                //   {
                //       Name = r.Name,
                //       Address = r.Address,
                //       Date = r.Date,
                //       ContactNum= r.ContactNum,
                //       EmailId=r.EmailId,
                //       Particular=r.Particular,
                //       NormalPrice=r.NormalPrice,
                //       OfferedPrice=r.OfferedPrice,
                //       TotalAmount=r.TotalAmount,
                //       BalanceAmount=r.BalanceAmount,
                //       //ModeOfPayment=r.ModeOfPayment,
                //       //DueDate=r.DueDate,
                //       //TermsAndCondition=r.TermsAndCondition,
                //       //ReceviedBy=r.ReceviedBy,
                //   }).ToPagedList(page, 10);

            //if (Request.IsAjaxRequest())
            //{
            //    return View("Index", model);
            //}

            //return View(model);
        }

        //
        // GET: /InvoiceBilling/Details/5

        public ActionResult Details(int id = 0)
        {
            InvoiceBilling invoicebilling = db.InvoiceBilling.Find(id);
            if (invoicebilling == null)
            {
                return HttpNotFound();
            }
            return View(invoicebilling);
        }
        public ActionResult Print(int id)
        {
            InvoiceBilling invoicebilling = db.InvoiceBilling.Find(id);
            if (invoicebilling == null)
            {
                return HttpNotFound();
            }
            return View(invoicebilling);
        }

        public ActionResult TermsAndConditon(int id=0)
        {
            InvoiceBilling invoicebilling = db.InvoiceBilling.Find(id);
            if (invoicebilling == null)
            {
                return HttpNotFound();
            }
            return View(invoicebilling);
        }


        //
        // GET: /InvoiceBilling/Create

        public ActionResult Create()
        {
            return View();
        }

        //
        // POST: /InvoiceBilling/Create

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(InvoiceBilling invoicebilling)
        {
            if (ModelState.IsValid)
            {
                db.InvoiceBilling.Add(invoicebilling);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(invoicebilling);
        }

        //
        // GET: /InvoiceBilling/Edit/5

        public ActionResult Edit(int id = 0)
        {
            InvoiceBilling invoicebilling = db.InvoiceBilling.Find(id);
            if (invoicebilling == null)
            {
                return HttpNotFound();
            }
            return View(invoicebilling);
        }

        //
        // POST: /InvoiceBilling/Edit/5

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(InvoiceBilling invoicebilling)
        {
            if (ModelState.IsValid)
            {
                db.Entry(invoicebilling).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(invoicebilling);
        }

        //
        // GET: /InvoiceBilling/Delete/5

        public ActionResult Delete(int id = 0)
        {
            InvoiceBilling invoicebilling = db.InvoiceBilling.Find(id);
            if (invoicebilling == null)
            {
                return HttpNotFound();
            }
            return View(invoicebilling);
        }

        //
        // POST: /InvoiceBilling/Delete/5

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            InvoiceBilling invoicebilling = db.InvoiceBilling.Find(id);
            db.InvoiceBilling.Remove(invoicebilling);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}