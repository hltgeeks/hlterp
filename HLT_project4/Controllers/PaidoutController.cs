﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using HLTERP.Models;

namespace HLTERP.Controllers
{
    public class PaidoutController : Controller
    {
        private HLTERPDB db = new HLTERPDB();

        //
        // GET: /Paidout/

        public ActionResult Index()
        {
            return View(db.PaidOuts.ToList());
        }

        //
        // GET: /Paidout/Details/5

        public ActionResult Details(int id = 0)
        {
            Paidout paidout = db.PaidOuts.Find(id);
            if (paidout == null)
            {
                return HttpNotFound();
            }
            return View(paidout);
        }

        //
        // GET: /Paidout/Create

        public ActionResult Create()
        {
            return View();
        }

        //
        // POST: /Paidout/Create

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(Paidout paidout)
        {
            if (ModelState.IsValid)
            {
                db.PaidOuts.Add(paidout);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(paidout);
        }

        //
        // GET: /Paidout/Edit/5

        public ActionResult Edit(int id = 0)
        {
            Paidout paidout = db.PaidOuts.Find(id);
            if (paidout == null)
            {
                return HttpNotFound();
            }
            return View(paidout);
        }

        //
        // POST: /Paidout/Edit/5

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(Paidout paidout)
        {
            if (ModelState.IsValid)
            {
                db.Entry(paidout).State =EntityState.Modified; ;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(paidout);
        }

        //
        // GET: /Paidout/Delete/5

        public ActionResult Delete(int id = 0)
        {
            Paidout paidout = db.PaidOuts.Find(id);
            if (paidout == null)
            {
                return HttpNotFound();
            }
            return View(paidout);
        }

        //
        // POST: /Paidout/Delete/5

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Paidout paidout = db.PaidOuts.Find(id);
            db.PaidOuts.Remove(paidout);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}