﻿using HLTERP.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace HLTERP.Controllers
{
    public class FileUploadController : Controller
    {
        //
        // GET: /FileUpload/

        public ActionResult FileUpload()
        {
            var model = new FileUpload();
            return View(model);
        }
        //
        // GET: /Enquiry/Details/5
        [HttpPost]
        public ActionResult FileUpload(HttpPostedFileBase file)
        {
            if (file != null && file.ContentLength > 0)
                try
                {
                    string path = Path.Combine(Server.MapPath("~/Content/Files"),
                   Path.GetFileName(file.FileName));
                    file.SaveAs(path);
                    ViewBag.Message = "File Uploaded successfully";
                }
                catch (Exception ex)
                {
                    ViewBag.Message = "Error:" + ex.Message.ToString();
                }
            else
            {
                ViewBag.Message = "File not uploaded. Please check your file.";
            }
            return View();

        }

    }
}
