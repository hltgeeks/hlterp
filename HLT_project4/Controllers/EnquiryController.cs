﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using HLTERP.Models;
using PagedList;

namespace HLTERP.Controllers
{
    public class EnquiryController : Controller
    {
        private HLTERPDB db = new HLTERPDB();


        public ActionResult Autocomplete(string term)
        {
            var model = db.Enquires
                .Where(r => r.Name.Contains(term))
                .Take(10)
                .Select(r => new { label = r.Name });

            return Json(model,JsonRequestBehavior.AllowGet);
        }
        //
        // GET: /Enquiry/
        [HttpGet]
        public ActionResult Index(string name=null,int page=1)
        {

            var model = db.Enquires
                        .OrderByDescending(r => r.Date)
                        .Where(r => name == null || r.Name.Contains(name))
                        .ToPagedList(page,3);
                        
            if (Request.IsAjaxRequest())
            {

                return PartialView("_Enquiries", model);

            }
            return View(model);

        }
        //
        // GET: /Enquiry/Details/5

        public ActionResult Details(int id = 0)
        {
            Enquiry enquiry = db.Enquires.Find(id);
            if (enquiry == null)
            {
                return HttpNotFound();
            }
            return View(enquiry);
        }

        //
        // GET: /Enquiry/Create

        public ActionResult Create()
        {

            Enquiry enquiry = new Enquiry();
            enquiry.Date = DateTime.Now;
            return View(enquiry);
        }

        //
        // POST: /Enquiry/Create

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(Enquiry enquiry)
        {
            if (ModelState.IsValid)
            {
                db.Enquires.Add(enquiry);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(enquiry);
        }

        //
        // GET: /Enquiry/Edit/5

        public ActionResult Edit(int id = 0)
        {
            Enquiry enquiry = db.Enquires.Find(id);
            if (enquiry == null)
            {
                return HttpNotFound();
            }
            return View(enquiry);
        }

        //
        // POST: /Enquiry/Edit/5

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(Enquiry enquiry)
        {
            if (ModelState.IsValid)
            {
                db.Entry(enquiry).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(enquiry);
        }

        //
        // GET: /Enquiry/Delete/5

        public ActionResult Delete(int id = 0)
        {
            Enquiry enquiry = db.Enquires.Find(id);
            if (enquiry == null)
            {
                return HttpNotFound();
            }
            return View(enquiry);
        }

        //
        // POST: /Enquiry/Delete/5

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Enquiry enquiry = db.Enquires.Find(id);
            db.Enquires.Remove(enquiry);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}