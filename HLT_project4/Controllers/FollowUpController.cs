﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using HLTERP.Models;

namespace HLTERP.Controllers
{
    public class FollowUpController : Controller
    {
        private HLTERPDB db = new HLTERPDB();

        //
        // GET: /FollowUp/

        public ActionResult Index([Bind(Prefix = "id")]int Enqid)
        {
            var enq = db.Enquires.Find(Enqid);
            if (enq != null)
            {
                return View(enq);
            }
            return HttpNotFound();
        }

        //
        // GET: /FollowUp/Details/5

        public ActionResult Details(int id = 0)
        {
            FollowUp followup = db.FollowUps.Find(id);
            if (followup == null)
            {
                return HttpNotFound();
            }
            return View(followup);
        }

        //
        // GET: /FollowUp/Create

        public ActionResult Create(int Enqid)
        {
            return View();
        }

        //
        // POST: /FollowUp/Create

        [HttpPost]
       
        public ActionResult Create(FollowUp curFollowup,int Enqid)
        {
            if (ModelState.IsValid)
            {
                curFollowup.Enquiry_id = Enqid;
                db.FollowUps.Add(curFollowup);
                db.SaveChanges();
                return RedirectToAction("Index", new { id = curFollowup.Enquiry_id });
            }

            return View(curFollowup);
        }

        //
        // GET: /FollowUp/Edit/5

        public ActionResult Edit(int id = 0)
        {
            FollowUp followup = db.FollowUps.Find(id);
            if (followup == null)
            {
                return HttpNotFound();
            }
            return View(followup);
        }

        //
        // POST: /FollowUp/Edit/5

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(FollowUp followup)
        {
            if (ModelState.IsValid)
            {
                db.Entry(followup).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(followup);
        }

        //
        // GET: /FollowUp/Delete/5

        public ActionResult Delete(int id = 0)
        {
            FollowUp followup = db.FollowUps.Find(id);
            if (followup == null)
            {
                return HttpNotFound();
            }
            return View(followup);
        }

        //
        // POST: /FollowUp/Delete/5

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            FollowUp followup = db.FollowUps.Find(id);
            db.FollowUps.Remove(followup);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}