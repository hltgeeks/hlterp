﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using HLTERP.Models;

namespace HLTERP.Controllers
{
    public class StartBatchController : Controller
    {
        private HLTERPDB db = new HLTERPDB();

        //
        // GET: /StartBatch/

        public ActionResult Index()
        {
            return View(db.StartBatches.ToList());
        }

        //
        // GET: /StartBatch/Details/5

        public ActionResult Details(int id = 0)
        {
            StartBatch startbatch = db.StartBatches.Find(id);
            if (startbatch == null)
            {
                return HttpNotFound();
            }
            return View(startbatch);
        }

        //
        // GET: /StartBatch/Create

        public ActionResult Create()
        {
           // if (db.StartBatches.Count() > 0)
            //{
                ViewBag.CourseList = new MultiSelectList(db.Courses, "CourseId", "CourseName");

                return View();
           // }
           // return HttpNotFound();
        }

        //
        // POST: /StartBatch/Create

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(StartBatch startbatch)
        {
            if (ModelState.IsValid)
            {
                db.StartBatches.Add(startbatch);
                db.SaveChanges();
                foreach(var courseId in startbatch.SelectedCourseList)
                {
                    var obj = new StartBatchAndCourse() { CourseId = courseId ,BatchId=startbatch.BatchId};
                }

                return RedirectToAction("Index");
            }

            return View(startbatch);
        }

        //
        // GET: /StartBatch/Edit/5

        public ActionResult Edit(int id = 0)
        {
            StartBatch startbatch = db.StartBatches.Find(id);
            if (startbatch == null)
            {
                return HttpNotFound();
            }
            startbatch.SelectedCourseList = db.StartBatchAndCourses.Where(m => m.BatchId == startbatch.BatchId).Select(a => a.CourseId).ToList();
            ViewBag.CoursesList = new MultiSelectList(db.Courses, "CourseId", "CourseName", startbatch.SelectedCourseList);
            return View(startbatch);
        }

        //
        // POST: /StartBatch/Edit/5

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(StartBatch startbatch)
        {
            if (ModelState.IsValid)
            {
                db.Entry(startbatch).State = EntityState.Modified;
                var exisitingCourseId=db.StartBatchAndCourses.Where(s=>s.BatchId==startbatch.BatchId).Select(s=>s.CourseId);

                var CourseIdToAdd = startbatch.SelectedCourseList.Except(exisitingCourseId);
                foreach(var CourseId in CourseIdToAdd)
                {
                    var obj = new StartBatchAndCourse() { CourseId = CourseId, BatchId = startbatch.BatchId };
                    db.StartBatchAndCourses.Add(obj);
                }
                var deleteCourseId = exisitingCourseId.Except(startbatch.SelectedCourseList);
                foreach(var courseId in deleteCourseId)
                {
                    var objCourseId=(from course in db.StartBatchAndCourses
                                         where course.CourseId==courseId && course.BatchId==startbatch.BatchId
                                         select course).Single();
                    db.StartBatchAndCourses.Remove(objCourseId);
                }
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(startbatch);
        }

        //
        // GET: /StartBatch/Delete/5

        public ActionResult Delete(int id = 0)
        {
            StartBatch startbatch = db.StartBatches.Find(id);
            if (startbatch == null)
            {
                return HttpNotFound();
            }
            return View(startbatch);
        }

        //
        // POST: /StartBatch/Delete/5

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            StartBatch startbatch = db.StartBatches.Find(id);
            db.StartBatches.Remove(startbatch);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        public ActionResult AddStudents()
        {
            return View();
        }



        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}