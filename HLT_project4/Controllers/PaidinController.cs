﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using HLTERP.Models;

namespace HLTERP.Controllers
{
    public class PaidinController : Controller
    {
        private HLTERPDB db = new HLTERPDB();

        //
        // GET: /Paidin/

        public ActionResult Index()
        {
            return View(db.PaidIns.ToList());
        }

        //
        // GET: /Paidin/Details/5

        public ActionResult Details(int id = 0)
        {
            Paidin paidin = db.PaidIns.Find(id);
            if (paidin == null)
            {
                return HttpNotFound();
            }
            return View(paidin);
        }

        //
        // GET: /Paidin/Create

        public ActionResult Create()
        {
            return View();
        }

        //
        // POST: /Paidin/Create

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(Paidin paidin)
        {
            if (ModelState.IsValid)
            {
                db.PaidIns.Add(paidin);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(paidin);
        }

        //
        // GET: /Paidin/Edit/5

        public ActionResult Edit(int id = 0)
        {
            Paidin paidin = db.PaidIns.Find(id);
            if (paidin == null)
            {
                return HttpNotFound();
            }
            return View(paidin);
        }

        //
        // POST: /Paidin/Edit/5

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(Paidin paidin)
        {
            if (ModelState.IsValid)
            {
                db.Entry(paidin).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(paidin);
        }

        //
        // GET: /Paidin/Delete/5

        public ActionResult Delete(int id = 0)
        {
            Paidin paidin = db.PaidIns.Find(id);
            if (paidin == null)
            {
                return HttpNotFound();
            }
            return View(paidin);
        }

        //
        // POST: /Paidin/Delete/5

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Paidin paidin = db.PaidIns.Find(id);
            db.PaidIns.Remove(paidin);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}