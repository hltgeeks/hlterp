﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using HLTERP.Models;
using PagedList;

namespace HLTERP.Controllers
{
    public class InvoiceController : Controller
    {
        private HLTERPDB db = new HLTERPDB();

        public ActionResult Index(string name = null, int page = 1)
        {
            var model = db.Invoices
                .OrderByDescending(r => r.Date)
                .Where(r => name == null || r.Name.Contains(name))
                .ToPagedList(page, 2);




            if (Request.IsAjaxRequest())
            {

                return PartialView("_Enquiries", model);

            }
            return View(model);
        }
                
                //
        // GET: /Invoice/

        public ActionResult Invoice()
        {
            return View(db.Invoices.ToList());
        }

    
        public ActionResult Print(int id )
        {
            Invoice invoice = db.Invoices.Find(id);
            if (invoice == null)
            {
                return HttpNotFound();
            }
            return View(invoice);
        }

        //
        // GET: /Invoice/Create

        public ActionResult Create()
        {
            return View();
        }

        //
        // POST: /Invoice/Create

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(Invoice invoice)
        {
            if (ModelState.IsValid)
            {
                db.Invoices.Add(invoice);
                
               //iNVOICE is also a Paidin
                    var payIn = new Paidin{ 
                    Date= invoice.Date, 
                    Course= invoice.Particular, 
                    PayerName= invoice.Name, 
                   PaidAmount= invoice.TotalAmount,
                   ReceiverName= invoice.ReceviedBy, 
                   ReceiptNo= invoice.ReceiptNo };

                 db.PaidIns.Add(payIn);
                db.SaveChanges();
                return RedirectToAction("Index");

            }

            return View(invoice);
        }

        //
        // GET: /Invoice/Edit/5

        public ActionResult Edit([Bind(Prefix = "id")] string receiptNo = null)
        {
            Invoice invoice = db.Invoices.Find(receiptNo);
            if (invoice == null)
            {
                return HttpNotFound();
            }
            return View(invoice);
        }

        //
        // POST: /Invoice/Edit/5

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(Invoice invoice)
        {
            if (ModelState.IsValid)
            {
                db.Entry(invoice).State = EntityState.Modified; ;
                var curPaidin = (Paidin)db.PaidIns.Where(r => r.ReceiptNo == invoice.ReceiptNo).FirstOrDefault();
                curPaidin.PaidAmount = invoice.TotalAmount;
                curPaidin.PayerName = invoice.Name;
                curPaidin.ReceiverName = invoice.ReceviedBy;
                curPaidin.Date = invoice.Date;
                curPaidin.Course = invoice.Particular;
                db.Entry(curPaidin).State = EntityState.Modified; ;

                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(invoice);
        }

        //
        // GET: /Invoice/Delete/5

        public ActionResult Delete(int id = 0)
        {
            Invoice invoice = db.Invoices.Find(id);
            if (invoice == null)
            {
                return HttpNotFound();
            }
            return View(invoice);
        }

        //
        // POST: /Invoice/Delete/5

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Invoice invoice = db.Invoices.Find(id);
            db.Invoices.Remove(invoice);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}