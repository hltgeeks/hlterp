﻿using HLTERP.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace HLTERP.Controllers
{
    public class AdmissionsController : Controller
    {
        private HLTERPDB db = new HLTERPDB();
        

        //
        // GET: /Admissions/

        public ActionResult Index()
        {
            return View(db.Admissions.ToList());
        }

        //
        // GET: /Admissions/Details/5

        public ActionResult Details(int id=0)
        {
          Admission  a = db.Admissions.Find(id);
           
            if (a==null)
            {
                return HttpNotFound();
            }
            return View(a);
        }

        //
        // GET: /Admissions/Create

        public ActionResult Create()
        {
            return View();
        }

        //
        // POST: /Admissions/Create

        [HttpPost]
        public ActionResult Create(Admission a)
        {
            try
            {
                db.Admissions.Add(a);
                db.SaveChanges();
                // TODO: Add insert logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View(a);
            }
        }

        //
        // GET: /Admissions/Edit/5

        public ActionResult Edit(int id=0)
        {
            Admission a = db.Admissions.Find(id);
            if(a==null)
            {
                return HttpNotFound();

            } 
            return View(a);
        }

        //
        // POST: /Admissions/Edit/5

        [HttpPost]
        public ActionResult Edit(Admission a)
        {
            try
            {
                // TODO: Add update logic here

                if (ModelState.IsValid);
                {
                    db.Entry(a).State = System.Data.Entity.EntityState.Modified;
                    db.SaveChanges();
                    return RedirectToAction("index");
                }
            }
            catch
            {
                return View(a);
            }
        }

        //
        // GET: /Admissions/Delete/5

        public ActionResult Delete(int id=0)
        {
            Admission a = db.Admissions.Find(id);
            if (a==null)
            {
                return HttpNotFound();
            }
            return View(a);
        }

        //
        // POST: /Admissions/Delete/5

        [HttpPost]
        public ActionResult DeleteConfirmed(int id)
        {
            Admission a = db.Admissions.Find(id);
            try
            {
               
                db.Admissions.Remove(a);
                db.SaveChanges();
                return RedirectToAction("Index");
                // TODO: Add delete logic here
            }
            catch
            {
                return View(a);
            }
        }
    }
}
